</$objtype/mkfile
BIN=/$objtype/bin

TARG=stm32up
OFILES=\
	stm32up.$O\

UPDATE=\
	mkfile\
	$HFILES\
	${OFILES:%.$O=%.c}\
	${TARG:%=/386/bin/%}\

</sys/src/cmd/mkone
