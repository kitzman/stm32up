# stm32up

This is a Plan9 tool to operate STM32 devices via the
UART protocol, when booting from the system memory.

The protocol is described in AN3155. For device-specific
information, you should read AN2606.

Commands:

info	- get device information and id

read	- read device memory

write	- write device memory

erase	- erase memory

flash	- erase everything, write, and go

go		- jump to address

readp	- protect memory from reads

readunp	- unprotect memory from reads

writeunp	- unprotect memory from writes

TODO: writep, csum

# Mirrors

https://git.disroot.org/kitzman/stm32up
http://shithub.us/kitzman/stm32up/HEAD/info.html
